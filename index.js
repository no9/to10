module.exports = function (val) {
    var retVal = '';
    for (var i = 0; i < val.length; i ++) {
        retVal += val.charCodeAt(i).toString(2);
    }
    return retVal;
}