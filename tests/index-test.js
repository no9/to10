var test = require('tap').test;
var tobin = require('../');

test('validate strings', function(t){
    t.plan(3);
    var a = tobin('a');
    t.equal(a, '1100001', 'single letter');
    var aa = tobin('aa');
    t.equal(aa, '11000011100001', 'two letters');
    var n = tobin('');
    t.equal(n, '', 'Nothing');
});