# to10

A simple library to convert a string to it's ones and zeros representation.

### usage

```
var to10 = require('to10');
var a = to10('a');
console.log(a)
'1100001'
```